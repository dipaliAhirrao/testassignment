package com.airfare.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import com.airfare.model.FareDetails;
import com.airfare.model.LocationPojo;
import com.airfare.model.Locations;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

@Service
public class AirfareService {

	private static final String GET_URL = "http://localhost:8080/airports";
	private static final String AUTH_URL = "http://localhost:8080/oauth/token";
	private static final String FARE_CALCULATE_URL = "http://localhost:8080/fares/{origin_code}/{destination_code}";
	public static final String TOKEN_REQUEST_URL = "url_link";
	public static final String CLIENT_ID = "travel-api-client";
	public static final String CLIENT_SECRET = "psw";

	/*
	 * Main Method: used to test the Location and fare calulator service services
	 * 
	 * public static void main(String[] args) throws IOException {
	 * 
	 * //Find List of Locations LocationPojo locPojo = sendGET(authGet());
	 * 
	 * //Fare Calculation fareCalculation("YOW","JRO",authGet());
	 * System.out.println("GET DONE");	 * 
	 * 
	 * }
	 */

	public List<Locations> getLocations() throws IOException {
		List<Locations> locations = new ArrayList<>();
		LocationPojo locPojo = sendGET(authGet());
		if (locPojo.get_embedded() != null && locPojo.get_embedded().getLocations() != null
				&& locPojo.get_embedded().getLocations().length > 0) {
			locations = Arrays.asList(locPojo.get_embedded().getLocations());
		}
		return locations;
	}

	
	public FareDetails getCalculatedFare(String originLoc,String destinationLoc) throws IOException {
		
		FareDetails fareDet = null;
		fareDet=fareCalculation(originLoc,destinationLoc,authGet());
		if (fareDet != null) {
			return fareDet;
		}
		return null;
	}
	 

	public static OAuth2AccessToken authGet() throws IOException {

		ClientCredentialsResourceDetails clientCredentialsResourceDetails = new ClientCredentialsResourceDetails();

		clientCredentialsResourceDetails.setAccessTokenUri(AUTH_URL);
		clientCredentialsResourceDetails.setAuthenticationScheme(AuthenticationScheme.header);
		clientCredentialsResourceDetails.setClientId("travel-api-client");
		clientCredentialsResourceDetails.setClientSecret("psw");
		clientCredentialsResourceDetails.setGrantType(GrantType.CLIENT_CREDENTIALS.toString());

		OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(clientCredentialsResourceDetails,
				new DefaultOAuth2ClientContext());
		OAuth2AccessToken token = oAuth2RestTemplate.getAccessToken();
		System.out.println("Auth response : " + oAuth2RestTemplate.getAccessToken());
		return token;
	}

	/*
	 * Method: sendGET(OAuth2AccessToken token) responsible for fetching all
	 * location input : valid access token
	 */
	public static LocationPojo sendGET(OAuth2AccessToken token) throws IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		JsonObject result = new JsonObject();
		HttpGet httpGet = new HttpGet(GET_URL);
		httpGet.addHeader("Authorization", "Bearer " + token);
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		ArrayList<LocationPojo> locationPojos = new ArrayList<LocationPojo>();

		System.out.println("GET Response Status:: " + httpResponse.getStatusLine().getStatusCode());
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();
		System.out.println(response.toString());
		httpClient.close();
		String countries = response.toString();
		ObjectMapper objectMapper = new ObjectMapper();
		LocationPojo locPojo = objectMapper.readValue(countries.getBytes(), LocationPojo.class);

		return locPojo;
	}

	/*
	 * This method is used to get Actual fare calculation Input: orgination Code and
	 * destination code.
	 */
	public static FareDetails fareCalculation(String origin, String destination, OAuth2AccessToken token)
			throws IOException {

		CloseableHttpClient httpClient = HttpClients.createDefault();
		JsonObject result = new JsonObject();
		String replaceOrigin = FARE_CALCULATE_URL.replace("{origin_code}", origin);
		String replaceDestination = replaceOrigin.replace("{destination_code}", destination);

		HttpGet httpGet = new HttpGet(replaceDestination);
		httpGet.addHeader("Authorization", "Bearer " + token);
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		System.out.println("GET Response Status:: " + httpResponse.getStatusLine().getStatusCode());
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();

		// print result
		System.out.println(response.toString());
		httpClient.close();

		String fareDetStr = response.toString();
		ObjectMapper objectMapper = new ObjectMapper();
		FareDetails locPojo = objectMapper.readValue(fareDetStr.getBytes(), FareDetails.class);

		return locPojo;

	}
}
