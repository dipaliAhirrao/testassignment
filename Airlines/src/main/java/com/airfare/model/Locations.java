package com.airfare.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Locations {

 	private Parent parent;

    private String code;

    private String name;

    private Coordinates coordinates;

    private String description;

    public Parent getParent ()
    {
        return parent;
    }

    public void setParent (Parent parent)
    {
        this.parent = parent;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Coordinates getCoordinates ()
    {
        return coordinates;
    }

    public void setCoordinates (Coordinates coordinates)
    {
        this.coordinates = coordinates;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [parent = "+parent+", code = "+code+", name = "+name+", coordinates = "+coordinates+", description = "+description+"]";
    }
}
