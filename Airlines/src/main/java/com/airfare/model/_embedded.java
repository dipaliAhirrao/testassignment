package com.airfare.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class _embedded {

	private Locations[] locations;

    public Locations[] getLocations ()
    {
        return locations;
    }

    public void setLocations (Locations[] locations)
    {
        this.locations = locations;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [locations = "+locations+"]";
    }
}
