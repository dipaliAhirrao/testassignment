package com.airfare.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.airfare.model.FareDetails;
import com.airfare.model.Locations;
import com.airfare.service.AirfareService;

@Controller
public class AirfareController {

	@Autowired
	AirfareService airFareService;

	@RequestMapping(method = RequestMethod.GET, value = "/searchLocations")
	public ModelAndView findLocations() throws IOException {
		 List<Locations> list = airFareService.getLocations();
		 ModelAndView map = new ModelAndView("launchPage");
		 map.addObject("locationLists", list);
		return map;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/fareCalculation")
	public String calculateFare(@RequestParam("oringin") String origin, @RequestParam("destination") String destination)
			throws IOException {
		FareDetails fareDet = airFareService.getCalculatedFare(origin, destination);
		ModelAndView map = new ModelAndView("FareDetails");
		map.addObject("fareDet", fareDet);
		return map.getViewName();
	}
}
